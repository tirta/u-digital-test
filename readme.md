# Installation

Clone this repository to your local machine:

```bash
git clone git@gitlab.com:tirta/u-digital-test.git .
```

Import the database to your MySQL Server. MySQL dump file name is database.sql.

Create wp-config.php from wp-config.local.php

Update the wp-config.php

## Theme development

Open the terminal and go to theme directory:

```bash
cd wp-content/themes/my-custom-theme
```

Install the required package and run the development environment:

```bash
npm install && npm run start
```

After finish update the theme, build and minify CSS and JS files.

```bash
npm run build
```

## Plugin development

Open the terminal and go to the plugin directory directory:

```bash
cd wp-content/plugins/wp-custom-block
```

Install the required package and run the development environment:

```bash
npm install && npm run start
```

After finish building plugin, build and minify CSS and JS files.

```bash
npm run build
```
