<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lazodevo_udigital' );

/** MySQL database username */
define( 'DB_USER', 'lazodevo_hired' );

/** MySQL database password */
define( 'DB_PASSWORD', 'u-r-hired!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm#e=g|?ryGf~LK^Q,op4:8G:C>H6qpD5JSiA`7.Lzw<(Tpwh3z<Dv@Z 9*iI%rtM' );
define( 'SECURE_AUTH_KEY',  'bKQ:h-L5r%*{yQ6+ZT=<ddpd&t(T9u7zaVeCZez!h+0{4^IR5~P~3}`Psow3/N#w' );
define( 'LOGGED_IN_KEY',    ':vjlEr/UC+9S83|r4pNK9?FU;Ko~;_nv#X{0T14$O-m^y{*BR*E,@$yy6xdbO&G0' );
define( 'NONCE_KEY',        '+,~Ezj5yw{9[4s*MVH*a*Lb]FW:T|eAUuH[SC[DsbJY;<@-t4NVf+l~d]d56hQ(K' );
define( 'AUTH_SALT',        'Gw}$;nYP{:LC|RRukCgp}Aw6Z 8rtsIo}r0Dkg8v(VI|Dol^)SmH-t^;cX# @;!k' );
define( 'SECURE_AUTH_SALT', 'xI`yJ|20FdnP::FoC:cHmFZ_<UJ6#G+%4~XJ4+I2~>Fq}O6L?GB.pwz*;|rLtk1c' );
define( 'LOGGED_IN_SALT',   '8x[iD;}hz&cv6WaH,;=<WabuW[U8d?w~$DGrK:=9{Za;pf#Q}[$>2;,YRvB?YH)f' );
define( 'NONCE_SALT',       'JPcBzF$eQ=mll[zt6497ccbcRX<YR~Z(;p.0k{ K4b_%[c+2>v~V<zw=N;F?.H0X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
