<?php
/**
 * Plugin Name: Custom Content Block for Wordpress
 */

// Create block category
function custom_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		[
            [
				'slug' => 'wp-custom-block',
				'title' => __( 'Custom Block', 'wp-custom-block' ),
			],
        ]
	);
}
add_filter( 'block_categories', 'custom_block_category', 10, 2);

// Register custom block
function wp_custom_block_register() {

    $asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

    wp_register_script(
        'wp-custom-block-script',
        plugins_url( 'build/index.js', __FILE__ ),
        $asset_file['dependencies'],
        $asset_file['version']
    );

	wp_register_style(
		'wp-custom-block-style',
		plugins_url( 'style.css', __FILE__ ),
		array(),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);


    register_block_type( 'wp-custom-block/breadcrumb', array(
        'editor_script' => 'wp-custom-block-script',
    ));

	register_block_type( 'wp-custom-block/column12', array(
        'editor_script' => 'wp-custom-block-script',
		'editor_style' => 'wp-custom-block-style',
    ));

	register_block_type( 'wp-custom-block/column6_5', array(
        'editor_script' => 'wp-custom-block-script',
    ));

}
add_action( 'init', 'wp_custom_block_register' );
