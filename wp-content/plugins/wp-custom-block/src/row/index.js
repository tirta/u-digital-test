
import { InnerBlocks } from '@wordpress/block-editor';
// import ClassName from 'classnames';

export const name = 'row'

export const setting = {
	title: 'Bootstrap Row',
  category: 'wp-custom-block',
	icon: 'universal-access-alt',
	edit: ( { className } ) => {
		return (
			<div className={ className }>
        <div className="container border border-success p-2">
          <div className="row">
            <InnerBlocks />
          </div>
        </div>
			</div>
		);
	},
	save: ( { className } ) => {
		return (
			<div className={ className }>
        <div className="container">
					<div className="row justify-content-between">
						<InnerBlocks.Content />
          </div>
        </div>
			</div>
		);
	},
}
