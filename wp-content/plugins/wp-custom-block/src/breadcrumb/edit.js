import {
  PlainText,
} from '@wordpress/block-editor';

export default ( props ) => {
  const { attributes } = props
  const { className } = props

  const onChange_itemHome = ( text ) => {
    props.setAttributes( { itemHome: text } );
  };

  const onChange_itemPage = ( text ) => {
    props.setAttributes( { itemPage: text } );
  };

  const onChange_itemSubPage = ( text ) => {
    props.setAttributes( { itemSubPage: text } );
  };

  return (
    <div className="d-flex flex-row border border-success p-2">
      <div className="mr-2">
        <PlainText
          className={ className }
          value={ attributes.itemHome }
          onChange={ onChange_itemHome }
          placeholder="Home Page Text"
        />
      </div>
      <div className="mr-2">/</div>
      <div className="mr-2">
        <PlainText
          className={ className }
          value={ attributes.itemPage }
          onChange={ onChange_itemPage }
          placeholder="Main Page Text"
        />
      </div>
      <div className="mr-2">/</div>
      <div>
        <PlainText
          className={ className }
          value={ attributes.itemSubPage }
          onChange={ onChange_itemSubPage }
          placeholder="Current Page"
        />
      </div>
    </div>
  );
}
