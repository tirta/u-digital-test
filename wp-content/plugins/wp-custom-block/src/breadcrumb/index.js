import edit from './edit'
import save from './save'

export const name = 'breadcrumb'

export const setting = {
	title: 'Bootstrap Breadcrumb',
	icon: 'universal-access-alt',
	category: 'wp-custom-block',
  attributes: {
    itemHome: {
			type: 'string',
			source: 'text',
			selector: '.item-home',
		},
    itemPage: {
			type: 'string',
			source: 'text',
			selector: '.item-page',
		},
    itemSubPage: {
			type: 'string',
			source: 'text',
			selector: '.item-subpage',
		},
	},
  example: {
    attributes: {
      itemHome: 'Home',
      itemPage: 'Who We Are',
      itemSubPage: 'Contact',
    }
  },
	edit,
	save,
}
