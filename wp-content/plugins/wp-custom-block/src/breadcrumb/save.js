
export default ( props ) => {
  const { attributes } = props
  const { className } = props

  return (
    <nav aria-label="breadcrumb" className={ className }>
      <div class="container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item item-home">{ attributes.itemHome }</li>
          <li class="breadcrumb-item item-page">{ attributes.itemPage }</li>
          <li class="breadcrumb-item item-subpage active" aria-current="page">{ attributes.itemSubPage }</li>
        </ol>
      </div>
    </nav>
  );
}
