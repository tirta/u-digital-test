
import { InnerBlocks, InspectorControls } from '@wordpress/block-editor';
import { Panel, PanelBody, PanelRow, SelectControl } from '@wordpress/components';

export const name = 'column'

export const setting = {
	title: 'Bootstrap Column',
  category: 'wp-custom-block',
	icon: 'universal-access-alt',
	attributes: {
		colClass: {
			type: 'string',
		},
	},
	edit: ( props ) => {
		const { attributes, setAttributes, className } = props;
		return (
			<div>
				<InspectorControls>
					<PanelBody
						title="Column Options"
						initialOpen={true}
					>
						<PanelRow>
							<SelectControl
								label="Column Size"
								value={attributes.colClass}
								options={[
									{label: "Column 1", value: 'col-md-1'},
									{label: "Column 2", value: 'col-md-2'},
									{label: "Column 3", value: 'col-md-3'},
									{label: "Column 4", value: 'col-md-4'},
									{label: "Column 5", value: 'col-md-5'},
									{label: "Column 6", value: 'col-md-6'},
									{label: "Column 7", value: 'col-md-7'},
									{label: "Column 8", value: 'col-md-8'},
									{label: "Column 9", value: 'col-md-9'},
									{label: "Column 10", value: 'col-md-10'},
									{label: "Column 11", value: 'col-md-11'},
									{label: "Column 12", value: 'col-md-12'},
								]}
								onChange={(newval) => setAttributes({ colClass: newval })}
							/>
						</PanelRow>
					</PanelBody>
				</InspectorControls>
				<div className="col border border-success p-2">
					<InnerBlocks/>
				</div>
			</div>
		);
	},
	save: ( props ) => {
		const { attributes, className } = props;
		return (
			<div className={attributes.colClass}>
				<InnerBlocks.Content/>
			</div>
		);
	},
}
