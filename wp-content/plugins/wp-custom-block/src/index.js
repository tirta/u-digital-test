import { registerBlockType } from '@wordpress/blocks';

import * as breadcrumb from './breadcrumb';
import * as row from './row';
import * as column from './column';

const blocks = [
    breadcrumb,
    row,
    column,
];

function registerBlock( block ) {
    const { name, setting } = block;
    registerBlockType( `wp-custom-block/${name}`, setting );
}

blocks.forEach( registerBlock );
