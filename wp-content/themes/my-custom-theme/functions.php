<?php

define('THE_THEME_NAME', 'my-custom-theme');

if ( ! function_exists( 'theme_setup' ) ) :
	function theme_setup() {

        // Wordpress managed title tag
		add_theme_support( 'title-tag' );

         // Enable post tumbnail
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'theme' ),
			)
		);

         // Enable output valid HTML5
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

        // Custom logo
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 45,
				// 'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'theme_setup' );

/*
* The theme customizer
*/
require_once get_template_directory() . '/includes/theme-customizer.php';

// Apply styles
function theme_scripts() {
    wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'compiled-style', get_template_directory_uri() . '/assets/css/starter.css', array(), wp_get_theme()->get( 'Version' ) );

	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'font-awesome-brand', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/brands.min.css', array(), wp_get_theme()->get( 'Version' ) );

	wp_enqueue_script( 'theme-compiled', get_template_directory_uri() . '/assets/js/starter.js', array('jquery'), wp_get_theme()->get( 'Version' ), true );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/**
 * Bootstrap nav walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/includes/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );
