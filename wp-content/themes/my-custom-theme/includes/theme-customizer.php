<?php

require_once __DIR__ . '/class-text-editor-custom-control.php';

function my_custom_theme_customize_register ($wp_customize) {

    $sectionName = 'my_custom_contact';

    $wp_customize->add_section($sectionName, array(
        'title'    => __('Contact Information', THE_THEME_NAME),
        'description' => '',
        'priority' => 120,
    ));

    /**
    * ADDRESS
    **/
    $wp_customize->add_setting( 'contact[address]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
    ) );

    $wp_customize->add_control( new Text_Editor_Custom_Control( $wp_customize, 'contact[address]', array(
        'label'   => 'Address',
        'section' => $sectionName,
        'settings'   => 'contact[address]',
        // 'priority' => 11
    ) ) );

    /**
    * PHONE
    **/
    $wp_customize->add_setting('contact[phone]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_phone', array(
        'label'      => __('Phone', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[phone]',
    ));

    /**
    * FAX
    **/
    $wp_customize->add_setting('contact[fax]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_fax', array(
        'label'      => __('Fax', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[fax]',
    ));

    /**
    * Facebook page
    **/
    $wp_customize->add_setting('contact[facebook]', array(
        'default'        => 'https://www.facebook.com/',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_facebook', array(
        'label'      => __('Facebook Page URL', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[facebook]',
    ));

    /**
    * Twitter
    **/
    $wp_customize->add_setting('contact[twitter]', array(
        'default'        => 'https://twitter.com/',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_twitter', array(
        'label'      => __('Twitter URL', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[twitter]',
    ));

    /**
    * Linked in
    **/
    $wp_customize->add_setting('contact[linkedin]', array(
        'default'        => 'https://www.linkedin.com/',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_linkedin', array(
        'label'      => __('LinkedIn Profile', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[linkedin]',
    ));

    /**
    * Pinterest
    **/
    $wp_customize->add_setting('contact[pinterest]', array(
        'default'        => 'https://pinterest.com/',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('contact_fax', array(
        'label'      => __('Pinterest URL', THE_THEME_NAME),
        'section'    => $sectionName,
        'settings'   => 'contact[pinterest]',
    ));
}

add_action('customize_register', 'my_custom_theme_customize_register');

if (!function_exists('reach_us_content')) {
    function reach_us_content () {
        $options = get_option('contact', []);

        // if (empty($options)) return ;

        $value = '';

        $value .= '<p>'.nl2br($options['address']).'</p>';
        $value .= "<p>
            Phone: <a href=\"tel:{$options['phone']}\">{$options['phone']}</a><br>
            Fax: <a href=\"tel:{$options['fax']}\">{$options['fax']}</a>
        </p>";

        $value .= "<p>&nbsp;</p>";

        $value .= "<p class=\"h1\">
            <a href=\"{$options['facebook']}\" class=\"socmed-icon\"><i class=\"fab fa-facebook-square\"></i></a>
            <a href=\"{$options['twitter']}\" class=\"socmed-icon\"><i class=\"fab fa-twitter-square\"></i></a>
            <a href=\"{$options['linkedin']}\" class=\"socmed-icon\"><i class=\"fab fa-linkedin\"></i></a>
            <a href=\"{$options['pinterest']}\" class=\"socmed-icon\"><i class=\"fab fa-pinterest-square\"></i></a>
        </p>";

        return $value;
    }
}

add_shortcode('reach_us_content', 'reach_us_content');
