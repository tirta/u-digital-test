<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="topbar py-2">
			<div class="container">
				<div class="d-flex justify-content-between">
					<div class="call-us-text">
						Call us now!
						<a href="tel:385.154.11.28.35" class="phone-cta">385.154.11.28.35</a>
					</div>
					<div class="">
						<a href="" class="call-us-text">Login</a>
						<a href="" class="phone-cta">Signup</a>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand site-branding" href="#">
					<?php the_custom_logo(); ?>
				</a>

				<?php
				$site_description = get_bloginfo( 'description', 'display' );
				if ( $site_description || is_customize_preview() ) :
					?>
					<p class="site-description d-none"><?php echo $site_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				<?php endif; ?>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
					<?php
					// wp_nav_menu(
					// 	array(
					// 		'theme_location' => 'menu-1',
					// 	)
					// );

					wp_nav_menu( array(
					    'theme_location'  => 'menu-1',
						'menu_id'        => 'primary-menu',
					    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
					    'container'       => 'div',
					    'container_class' => 'collapse navbar-collapse',
					    'container_id'    => 'navbarSupportedContent',
					    'menu_class'      => 'navbar-nav ml-auto',
					    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
					    'walker'          => new WP_Bootstrap_Navwalker(),
					) );
					?>
				<!-- </div> -->
			</div>
		</nav>
	</header>
