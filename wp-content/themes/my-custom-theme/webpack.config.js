const path = require('path');

module.exports = {
  entry: './js/app.js',
  output: {
    filename: 'starter.js',
    path: path.resolve(__dirname, 'assets/js'),
  },
  mode: 'production'
};
